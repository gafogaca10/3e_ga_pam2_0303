import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder/Inbox',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./components/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./components/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'cadastro-usuario',
    loadChildren: () => import('./components/cadastro-usuario/cadastro-usuario.module').then( m => m.CadastroUsuarioPageModule)
  },
  {
    path: 'cadastro-clientes',
    loadChildren: () => import('./components/cadastro-clientes/cadastro-clientes.module').then( m => m.CadastroClientesPageModule)
  },
  {
    path: 'cadastro-motos',
    loadChildren: () => import('./components/cadastro-motos/cadastro-motos.module').then( m => m.CadastroMotosPageModule)
  },
  {
    path: 'touring',
    loadChildren: () => import('./components/touring/touring.module').then( m => m.TouringPageModule)
  },
  {
    path: 'sport',
    loadChildren: () => import('./components/sport/sport.module').then( m => m.SportPageModule)
  },
  {
    path: 'scooter',
    loadChildren: () => import('./components/scooter/scooter.module').then( m => m.ScooterPageModule)
  },
  {
    path: 'trail',
    loadChildren: () => import('./components/trail/trail.module').then( m => m.TrailPageModule)
  },
  {
    path: 'custom',
    loadChildren: () => import('./components/custom/custom.module').then( m => m.CustomPageModule)
  },
  {
    path: 'street',
    loadChildren: () => import('./components/street/street.module').then( m => m.StreetPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
